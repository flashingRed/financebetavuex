import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import dotenv from 'dotenv'
import VueWait from 'vue-wait'

Vue.use(VueWait)
 

dotenv.config()

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: function(h) { return h(App) },
    wait: new VueWait(),
}).$mount('#app')