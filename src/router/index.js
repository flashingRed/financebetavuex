import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Enter from '../views/Enter.vue'
import SighIn from '../views/SighIn.vue'
import Wallets from '../views/Wallets.vue'
import TransAct from '../views/TransAct.vue'
import AddWallet from '../views/AddWallet.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/Enter',
        name: 'Enter',
        component: Enter
    },
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/SIGN_IN',
        name: 'Sign',
        component: SighIn
    },
    {
        path: '/Wallets',
        name: 'Wallets',
        component: Wallets
    },
    {
        path: '/TransAct',
        name: 'TransAct',
        component: TransAct
    },
    {
        path: '/AddWallet',
        name: 'AddWallet',
        component: AddWallet
    },
    
    
]

const router = new VueRouter({
    routes
})

export default router